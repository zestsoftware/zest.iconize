from setuptools import setup, find_packages
import sys, os

version = '0.2.dev0'

setup(name='zest.iconize',
      version=version,
      description="Favicon generator",
      long_description="""\
""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='favicon favicon.ico generator apple-touch-icon mstile png',
      author='Diederik Veeze',
      author_email='d.veeze@zestsoftware.nl',
      url='',
      license='',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'Pillow'
      ],
      entry_points={
          'console_scripts': [
               'iconize = zest.iconize.iconize:main'
               ]
          }
      )
