#!/usr/bin/env python

import argparse
import subprocess
import commands
import sys
import json

from PIL import Image

default_icon_formats = {
    'favicon': {
        'name': 'favicon',
        'extension': 'ico',
        'type': 'x-icon'
    },
    'general': [
        {
            'name': 'favicon',
            'sizes': [16,32,96],
            'extension': 'png',
            'type': 'png',
            'rel': 'icon'
        },
        {
            'name': 'apple-touch-icon',
            'sizes': [57,60,72,76,114,120,144],
            'extension': 'png',
            'type': 'png',
            'rel': 'apple-touch-icon'
        },
        {
            'name': 'android-chrome',
            'sizes' : [36, 48, 72, 96, 144, 192],
            'type': 'png',
            'extension': 'png',
            'rel': 'icon'
        }
    ],
    'mstiles': {
        'name': 'mstile',
        'sizes': [(144,144)],
        'extension': 'png'
    }
}

class Iconizer():
    def __init__(self):
        self.args = self.parse_arguments()
        self.opti_images = []
        if self.args.optimize and commands.getstatusoutput("optipng")[0]:
            sys.stderr.write('[ERROR]: could not find optipng\n')
            sys.exit()
    
    def __del__(self):
        if not hasattr(self, 'args'):
            return
        if self.args.optimize:
            self.optimize(self.opti_images)

    def parse_arguments(self):
        parser = argparse.ArgumentParser(
                prog='iconize.py',
                description='create favicons from source image'
        )
        parser.add_argument('-o', '--optimize', action='store_true', help='optimize png images (requires optipng)')
        parser.add_argument('-p', '--prefix', help='path prefix for the icon files')
        parser.add_argument('-c', '--tile-color', help='Background color for MS tiles')
        parser.add_argument('-f', '--formats', help='use custom formats file', type=argparse.FileType('r'), nargs=1)
        parser.add_argument('-d', '--dump-formats', action='store_true', help='dump an example formats file')
        parser.add_argument('input_image', type=argparse.FileType('r'), nargs='?')
        

        args = parser.parse_args()
        
        if args.dump_formats:
            open('example-formats.json', 'w').write(json.dumps(default_icon_formats, indent=4))
            sys.exit(0)

        if args.formats:
            try:
                self.formats = json.load(args.formats[0])
            except ValueError:
                sys.stderr.write('Invalid json file format')
        else:
            self.formats = default_icon_formats
        
        if not args.input_image:
            parser.print_help()
            sys.exit(1)

        return args

    def open_image(self, image_file):
        try :
            image = Image.open(image_file)
        except:
            sys.stderr.write('[ERROR]: Could not open image file\n')
            sys.exit(1)
        return image

    def generate(self):
        self.favicon()
        self.general()
        self.mstiles()

    def favicon(self):
        try:
            favicon_format = self.formats['favicon']
        except KeyError:
            return
        extension = self.formats['favicon']['extension']
        filename = '%s.%s' % (favicon_format['name'], extension)
        image = self.open_image(self.args.input_image)
        image.thumbnail((255,255), Image.ANTIALIAS)
        background = Image.new('RGBA', (255, 255), (255, 255, 255, 0))
        background.paste(image, ((255 - image.size[0]) / 2, (255 - image.size[1]) / 2))
        background.save(filename, extension)
        print('<link rel="shortcut icon" type="image/%s" href="%s/%s" />' % (
            self.formats['favicon']['type'], self.args.prefix or '', filename))

    def general(self):
        orig_image = self.open_image(self.args.input_image)
        try:
            general_formats = self.formats['general']
        except KeyError:
            return
        for format in general_formats:
            for size in format['sizes']:
                image = orig_image.convert('RGBA')
                filename = '%s-%ix%i.%s' % (format['name'], size, size, format['extension'])
                image.thumbnail((size, size), Image.ANTIALIAS)
                background = Image.new('RGBA', (size, size), (255, 255, 255, 0))
                background.paste(image, ((size - image.size[0]) / 2, (size - image.size[1]) / 2))
                background.save(filename, format['extension'])
                if format['extension'] == 'png':
                    self.opti_images.append(filename)
                print('<link rel="%s" type="image/%s" href="%s/%s" sizes="%ix%i" />' % (
                    format['rel'], format['type'], self.args.prefix or '', filename, size, size))

    def mstiles(self):
        orig_image = self.open_image(self.args.input_image)
        if not self.args.tile_color:
            bgcolor = "%0.2X%0.2X%0.2X" % self.avarage_color(orig_image)
        else:
            bgcolor = self.args.tile_color
        try:
            mstiles = self.formats['mstiles']
        except KeyError:
            return
        print('<meta name="msapplication-TileColor" content="#%s" />' % (bgcolor))
        for size in mstiles['sizes']:
            image = orig_image.convert('RGBA')
            filename = '%s-%ix%i.%s' % (mstiles['name'], size[0], size[1], mstiles['extension'])
            image.thumbnail(size, Image.ANTIALIAS)
            mask = image.convert("L")
            tile = Image.new('RGBA', image.size, (255,255,255,255))
            tile.putalpha(mask)
            tile.save(filename)
            if mstiles['extension'] == 'png':
                self.opti_images.append(filename)
            print('<meta name="msapplication-TileImage" content="%s/%s" />' % (self.args.prefix or '', filename))

    def avarage_color(self, image):
        image = image.convert('RGBA')
        image_data = image.load()
        r, g, b, i = 0, 0, 0, 0
        for x in range(image.size[0]):
            for y in range(image.size[1]):
                tempr, tempg, tempb, alpha = image_data[x,y]
                if alpha > 128:
                    r += tempr
                    g += tempg
                    b += tempb
                    i += 1
        return (r / i, g / i, b /i)


    def optimize(self, png_files):
        command = ['optipng', '-strip', 'all']
        command.extend(png_files)
        p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()


def main():
    iconizer = Iconizer()
    iconizer.generate()

if __name__ == '__main__':
    main()
