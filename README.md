# Usage #
```
usage: iconize.py [-h] [-o] [-p PREFIX] [-c TILE_COLOR] [-f FORMATS] [-d]
                  [input_image]

create favicons from source image

positional arguments:
  input_image

optional arguments:
  -h, --help            show this help message and exit
  -o, --optimize        optimize png images (requires optipng)
  -p PREFIX, --prefix PREFIX
                        path prefix for the icon files
  -c TILE_COLOR, --tile-color TILE_COLOR
                        Background color for MS tiles
  -f FORMATS, --formats FORMATS
                        use custom formats file
  -d, --dump-formats    dump an example formats file
```